class PhotosController < ApplicationController
  before_filter :get_daily_photo, only: [:index]
  helper_method :photo_height, :photo_width

  # GET /photos
  # GET /photos.json
  def index  
    @last_five = DailyPhoto.last(6)
    @last_five.pop
    @last_five = @last_five.reverse
  end

  def photo_height(photo)
    img = MiniMagick::Image.open("app/assets/images/rowan-pics/#{photo}")
    @height = img.height
  end

  def photo_width(photo)
    img = MiniMagick::Image.open("app/assets/images/rowan-pics/#{photo}")
    @width = img.width
  end

  def create_daily_photo
    photos = Photo.available_photos
    photo = File.basename(photos.sample)
      
    @photo = DailyPhoto.where(date: Time.current.to_date).first_or_create do |d|
      d.photo = photo
    end
  end

  private
    def get_daily_photo
      counter = Counter.first_or_create
      if Photo.available_photos.blank?
        counter.update_attribute("current_count", counter.current_count += 1)
        create_daily_photo
      else
        create_daily_photo
      end
    end
end