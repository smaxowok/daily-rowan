class Photo
	def self.all
		Dir.glob(File.join(Rails.root, "app", "assets", "images", "rowan-pics", "*.jpg"))
	end

	def self.new_photos
		photos = []
		Photo.all.each do |p|
			photos.push(p) unless photo_displayed?(File.basename(p))
		end
		photos
	end

	def self.upcoming_photos
		counter = Counter.first_or_create
		photos = []
		Photo.all.each do |p|
			photos.push(p) if photo_displayed?(File.basename(p)) && display_count(File.basename(p)) < counter.current_count
		end
		photos
	end

	def self.photo_displayed?(photo)
		display_count(photo) > 0 ? true : false
	end

	def self.display_count(photo)
		DailyPhoto.where(photo: photo).count
	end

	def self.available_photos
		new_photos.blank? ? upcoming_photos : new_photos
	end
	
end
