class AddDefaultValueToCounter < ActiveRecord::Migration
  def change
  	change_column :counters, :current_count, :integer, default: 1
  end
end
