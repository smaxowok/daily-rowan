class CreateDailyPhotos < ActiveRecord::Migration
  def change
    create_table :daily_photos do |t|
      t.date :date
      t.references :photo, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
