class PhotoDumpRename < ActiveRecord::Migration
  def change
  	remove_reference :daily_photos, :photo, index: true, foreign_key: true
  	add_column :daily_photos, :photo, :string
  	drop_table :photos
  end
end
