class CreateCounters < ActiveRecord::Migration
  def change
    create_table :counters do |t|
      t.integer :current_count
    end
  end
end
